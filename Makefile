CC = gcc
CLIB = ar
CFLAGS = -Wall -O3
LDFLAGS = -pthread
LIBFLAGS = rcs
PREFIJO = fjgc_
LIBRERIA = lib
OTROS = splash
SDIR = src
ODIR = obj
LIBDIR = lib
DIRECTORIOS=$(ODIR) $(LIBDIR)
APPS =	app.null \
	app.pthread \
	app.test_and_set \
	app.test_and_set_backoff \
	app.test_and_test_and_set \
	app.ticket_lock \
	app.clh_tas

all: $(APPS)

app.%: $(ODIR)/app.%.o $(LIBDIR)/$(PREFIJO)$(LIBRERIA)%.a | $(DIRECTORIOS)
	$(CC) $(CFLAGS) $^ $(LDFLAGS) -o $@

$(ODIR)/app.%.o: $(SDIR)/aplicacion.c
	$(CC) $(CFLAGS) -c $^ -o $@

$(LIBDIR)/$(PREFIJO)$(LIBRERIA)%.a: $(ODIR)/$(PREFIJO)$(LIBRERIA).o $(ODIR)/$(PREFIJO)$(OTROS).o $(ODIR)/$(PREFIJO)%.o | $(DIRECTORIOS)
	$(CLIB) $(LIBFLAGS) $@ $^

.PRECIOUS: $(LIBDIR)/$(PREFIJO)$(LIBRERIA)%.a

$(ODIR)/$(PREFIJO)$(OTROS).o: $(SDIR)/$(PREFIJO)$(OTROS).c $(SDIR)/$(PREFIJO)$(LIBRERIA).h | $(DIRECTORIOS)
	$(CC) $(CFLAGS) -c $(SDIR)/$(PREFIJO)$(OTROS).c $(LDFLAGS) -o $@

$(ODIR)/$(PREFIJO)$(LIBRERIA).o: $(SDIR)/$(PREFIJO)$(LIBRERIA).c $(SDIR)/$(PREFIJO)$(LIBRERIA).h | $(DIRECTORIOS)
	$(CC) $(CFLAGS) -c $(SDIR)/$(PREFIJO)$(LIBRERIA).c $(LDFLAGS) -o $@

$(ODIR)/$(PREFIJO)%.o: $(SDIR)/$(PREFIJO)%.c | $(DIRECTORIOS)
	$(CC) $(CFLAGS) -c $^ $(LDFLAGS) -o $@

$(ODIR) $(LIBDIR):
	mkdir -p $@

.PHONY: clean
clean:
	rm -rf $(ODIR) $(LIBDIR) $(APPS) *~
