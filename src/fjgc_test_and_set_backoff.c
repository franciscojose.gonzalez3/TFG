#include <errno.h>
#include <stdlib.h>
#include <string.h>

#define ENOMEM 12 /*No hay memoria disponible*/
#define PUNTOFIJO 8

struct fjgc_mutex{
	unsigned char _cerrojo;
};

void (*backoff) (struct fjgc_mutex*);
unsigned int valorInicio=1;
unsigned int cotaSuperior;

int fjgc_lock_release(struct fjgc_mutex* cerrojo);

void fjgc_backoff_2(struct fjgc_mutex* cerrojo){

	unsigned char reg1=1;
	unsigned int espera=valorInicio << PUNTOFIJO;
	unsigned int esperado=0;
	unsigned int stride = 1 << PUNTOFIJO;

	__asm__ __volatile (
		"xchg %[aReg1], %[aCerrojo]\n\t"
		: [aCerrojo] "=m" (*cerrojo), [aReg1] "=r" (reg1)
		: "1" (reg1)
	);

	while(reg1){

		while(esperado<espera){

			esperado+=stride;
		}

		esperado=0;
		espera+=espera;

		__asm__ __volatile (
			"xchg %[aReg1], %[aCerrojo]\n\t"
			: [aCerrojo] "=m" (*cerrojo), [aReg1] "=r" (reg1)
			: "1" (reg1)
			);

	}
}

void fjgc_backoff_1_5(struct fjgc_mutex* cerrojo){

	unsigned char reg1=1;
	unsigned int espera=valorInicio << PUNTOFIJO;
	unsigned int esperado=0;
	unsigned int stride = 1 << PUNTOFIJO;

	__asm__ __volatile (
		"xchg %[aReg1], %[aCerrojo]\n\t"
		: [aCerrojo] "=m" (*cerrojo), [aReg1] "=r" (reg1)
		: "1" (reg1)
	);

	while(reg1){

		while(esperado<espera){

			esperado+=stride;
		}

		esperado=0;
		espera+=espera>>1;

		__asm__ __volatile (
			"xchg %[aReg1], %[aCerrojo]\n\t"
			: [aCerrojo] "=m" (*cerrojo), [aReg1] "=r" (reg1)
			: "1" (reg1)
			);

	}
}

void fjgc_backoff_1_75(struct fjgc_mutex* cerrojo){

	unsigned char reg1=1;
	unsigned int espera=valorInicio << PUNTOFIJO;
	unsigned int esperado=0;
	unsigned int stride = 1 << PUNTOFIJO;

	__asm__ __volatile (
		"xchg %[aReg1], %[aCerrojo]\n\t"
		: [aCerrojo] "=m" (*cerrojo), [aReg1] "=r" (reg1)
		: "1" (reg1)
	);

	while(reg1){

		while(esperado<espera){

			esperado+=stride;
		}

		esperado=0;
		espera+=(espera>>1)+(espera>>2);

		__asm__ __volatile (
			"xchg %[aReg1], %[aCerrojo]\n\t"
			: [aCerrojo] "=m" (*cerrojo), [aReg1] "=r" (reg1)
			: "1" (reg1)
			);

	}
}

void fjgc_backoff_2_25(struct fjgc_mutex* cerrojo){

	unsigned char reg1=1;
	unsigned int espera=valorInicio << PUNTOFIJO;
	unsigned int esperado=0;
	unsigned int stride = 1 << PUNTOFIJO;

	__asm__ __volatile (
		"xchg %[aReg1], %[aCerrojo]\n\t"
		: [aCerrojo] "=m" (*cerrojo), [aReg1] "=r" (reg1)
		: "1" (reg1)
	);

	while(reg1){

		while(esperado<espera){

			esperado+=stride;
		}

		esperado=0;
		espera+=espera+(espera>>2);

		__asm__ __volatile (
			"xchg %[aReg1], %[aCerrojo]\n\t"
			: [aCerrojo] "=m" (*cerrojo), [aReg1] "=r" (reg1)
			: "1" (reg1)
			);

	}
}

void fjgc_backoff_2_5(struct fjgc_mutex* cerrojo){

	unsigned char reg1=1;
	unsigned int espera=valorInicio << PUNTOFIJO;
	unsigned int esperado=0;
	unsigned int stride = 1 << PUNTOFIJO;

	__asm__ __volatile (
		"xchg %[aReg1], %[aCerrojo]\n\t"
		: [aCerrojo] "=m" (*cerrojo), [aReg1] "=r" (reg1)
		: "1" (reg1)
	);

	while(reg1){

		while(esperado<espera){

			esperado+=stride;
		}

		esperado=0;
		espera+=espera+(espera>>1);

		__asm__ __volatile (
			"xchg %[aReg1], %[aCerrojo]\n\t"
			: [aCerrojo] "=m" (*cerrojo), [aReg1] "=r" (reg1)
			: "1" (reg1)
			);

	}
}

void fjgc_backoff_2_cota(struct fjgc_mutex* cerrojo){

	unsigned char reg1=1;
	unsigned int espera=valorInicio << PUNTOFIJO;
	unsigned int esperado=0;
	unsigned int stride = 1 << PUNTOFIJO;

	__asm__ __volatile (
		"xchg %[aReg1], %[aCerrojo]\n\t"
		: [aCerrojo] "=m" (*cerrojo), [aReg1] "=r" (reg1)
		: "1" (reg1)
	);

	while(reg1){

		while(esperado<espera){

			esperado+=stride;
		}

		esperado=0;
		if(espera<cotaSuperior) espera+=espera;

		__asm__ __volatile (
			"xchg %[aReg1], %[aCerrojo]\n\t"
			: [aCerrojo] "=m" (*cerrojo), [aReg1] "=r" (reg1)
			: "1" (reg1)
			);

	}
}

void fjgc_backoff_1_5_cota(struct fjgc_mutex* cerrojo){

	unsigned char reg1=1;
	unsigned int espera=valorInicio << PUNTOFIJO;
	unsigned int esperado=0;
	unsigned int stride = 1 << PUNTOFIJO;

	__asm__ __volatile (
		"xchg %[aReg1], %[aCerrojo]\n\t"
		: [aCerrojo] "=m" (*cerrojo), [aReg1] "=r" (reg1)
		: "1" (reg1)
	);

	while(reg1){

		while(esperado<espera){

			esperado+=stride;
		}

		esperado=0;
		if(espera<cotaSuperior) espera+=espera>>1;

		__asm__ __volatile (
			"xchg %[aReg1], %[aCerrojo]\n\t"
			: [aCerrojo] "=m" (*cerrojo), [aReg1] "=r" (reg1)
			: "1" (reg1)
			);

	}
}

void fjgc_backoff_1_75_cota(struct fjgc_mutex* cerrojo){

	unsigned char reg1=1;
	unsigned int espera=valorInicio << PUNTOFIJO;
	unsigned int esperado=0;
	unsigned int stride = 1 << PUNTOFIJO;

	__asm__ __volatile (
		"xchg %[aReg1], %[aCerrojo]\n\t"
		: [aCerrojo] "=m" (*cerrojo), [aReg1] "=r" (reg1)
		: "1" (reg1)
	);

	while(reg1){

		while(esperado<espera){

			esperado+=stride;
		}

		esperado=0;
		if(espera<cotaSuperior) espera+=(espera>>1)+(espera>>2);

		__asm__ __volatile (
			"xchg %[aReg1], %[aCerrojo]\n\t"
			: [aCerrojo] "=m" (*cerrojo), [aReg1] "=r" (reg1)
			: "1" (reg1)
			);

	}
}

void fjgc_backoff_2_25_cota(struct fjgc_mutex* cerrojo){

	unsigned char reg1=1;
	unsigned int espera=valorInicio << PUNTOFIJO;
	unsigned int esperado=0;
	unsigned int stride = 1 << PUNTOFIJO;

	__asm__ __volatile (
		"xchg %[aReg1], %[aCerrojo]\n\t"
		: [aCerrojo] "=m" (*cerrojo), [aReg1] "=r" (reg1)
		: "1" (reg1)
	);

	while(reg1){

		while(esperado<espera){

			esperado+=stride;
		}

		esperado=0;
		if(espera<cotaSuperior) espera+=espera+(espera>>2);

		__asm__ __volatile (
			"xchg %[aReg1], %[aCerrojo]\n\t"
			: [aCerrojo] "=m" (*cerrojo), [aReg1] "=r" (reg1)
			: "1" (reg1)
			);

	}
}

void fjgc_backoff_2_5_cota(struct fjgc_mutex* cerrojo){

	unsigned char reg1=1;
	unsigned int espera=valorInicio << PUNTOFIJO;
	unsigned int esperado=0;
	unsigned int stride = 1 << PUNTOFIJO;

	__asm__ __volatile (
		"xchg %[aReg1], %[aCerrojo]\n\t"
		: [aCerrojo] "=m" (*cerrojo), [aReg1] "=r" (reg1)
		: "1" (reg1)
	);

	while(reg1){

		while(esperado<espera){

			esperado+=stride;
		}

		esperado=0;
		if(espera<cotaSuperior) espera+=espera+(espera>>1);

		__asm__ __volatile (
			"xchg %[aReg1], %[aCerrojo]\n\t"
			: [aCerrojo] "=m" (*cerrojo), [aReg1] "=r" (reg1)
			: "1" (reg1)
			);

	}
}

//Inicializa cerrojo a 0
int fjgc_lock_init(struct fjgc_mutex** cerrojo){

	*cerrojo = malloc(sizeof(struct fjgc_mutex));
	if(cerrojo==NULL) return ENOMEM;

	char* inicio=getenv("FJGC_INICIO");

	if(inicio!=NULL && inicio[0]!='\0'){

		char* comprobacion=NULL;
		int aux;
		errno=0;
		aux = strtol(inicio,&comprobacion,10);
		if(errno || comprobacion==inicio || *comprobacion!='\0' || aux<1) valorInicio=1;
		else valorInicio=aux;
	}

	char* cota=getenv("FJGC_COTA");
	int cotaValida=0;

	if(cota!=NULL && cota[0]!='\0'){

		char* comprobacion=NULL;
		int aux;
		errno=0;
		aux = strtol(cota,&comprobacion,10);
		if(!(errno || comprobacion==inicio || *comprobacion!='\0' || aux<0)){
			cotaValida=1;
			cotaSuperior=aux;
		}
	}

	char* exponente=getenv("FJGC_EXPONENTE");

	if(!cotaValida){
		backoff = &fjgc_backoff_2;

		if(exponente!=NULL){
			if(!strcmp(exponente, "1.5")) backoff = &fjgc_backoff_1_5;
			else if(!strcmp(exponente, "1.75")) backoff = &fjgc_backoff_1_75;
			else if(!strcmp(exponente, "2.25")) backoff = &fjgc_backoff_2_25;
			else if(!strcmp(exponente, "2.5")) backoff = &fjgc_backoff_2_5;
		}
	}
	else{
		backoff = &fjgc_backoff_2_cota;

		if(exponente!=NULL){
			if(!strcmp(exponente, "1.5")) backoff = &fjgc_backoff_1_5_cota;
			else if(!strcmp(exponente, "1.75")) backoff = &fjgc_backoff_1_75_cota;
			else if(!strcmp(exponente, "2.25")) backoff = &fjgc_backoff_2_25_cota;
			else if(!strcmp(exponente, "2.5")) backoff = &fjgc_backoff_2_5_cota;
		}
	}

	return fjgc_lock_release(*cerrojo);
}

//Implementa la primitiva test_and_set con backoff
int fjgc_lock_acquire(struct fjgc_mutex* cerrojo){

	backoff(cerrojo);
	return 0;
}

//Establece el valor del cerrojo a 0
int fjgc_lock_release(struct fjgc_mutex* cerrojo){

	__asm__ __volatile(

		"movb $0, %[aCerrojo]\n\t"

		: [aCerrojo] "=m" (*cerrojo)
	);

	return 0;
}

//Libera la memoria asociada al cerrojo
int fjgc_lock_destroy(struct fjgc_mutex* cerrojo){

	free(cerrojo);
	return 0;
}
