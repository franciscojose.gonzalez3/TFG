#include <pthread.h>
#include <stdlib.h>

#define ENOMEM 12 /*No hay memoria disponible*/

struct fjgc_mutex{
	pthread_mutex_t _cerrojo;
};

int fjgc_lock_init(struct fjgc_mutex** cerrojo){

	*cerrojo = malloc(sizeof(struct fjgc_mutex));
	if(cerrojo==NULL) return ENOMEM;
	return pthread_mutex_init(&(*cerrojo)->_cerrojo, NULL);
}

int fjgc_lock_acquire(struct fjgc_mutex* cerrojo){

	return pthread_mutex_lock(&cerrojo->_cerrojo);
}

int fjgc_lock_release(struct fjgc_mutex* cerrojo){

	return pthread_mutex_unlock(&cerrojo->_cerrojo);
}

int fjgc_lock_destroy(struct fjgc_mutex* cerrojo){

	int error=pthread_mutex_destroy(&cerrojo->_cerrojo);
	free(cerrojo);
	return error;
}
