#include <stdlib.h>
#include "fjgc_lib.h"

#define ENOMEM 12 /*No hay memoria disponible*/

struct fjgc_sem{
	struct fjgc_mutex* _cerrojo;
	volatile unsigned int _valor;
};

int fjgc_sem_init(struct fjgc_sem** semaforo, int valor){

	*semaforo = malloc(sizeof(struct fjgc_sem));
	if(*semaforo==NULL) return ENOMEM;
	(*semaforo)->_valor=valor;
	return fjgc_lock_init(&(*semaforo)->_cerrojo);
}

int fjgc_sem_post(struct fjgc_sem* semaforo){

	int estado=0;
	estado|=fjgc_lock_acquire(semaforo->_cerrojo);
	semaforo->_valor++;
	estado|=fjgc_lock_release(semaforo->_cerrojo);
	return estado;
}

int fjgc_sem_wait(struct fjgc_sem* semaforo){

	int estado=0;
	char fin=0;

	while(!fin){
		if(semaforo->_valor > 0){

		estado|=fjgc_lock_acquire(semaforo->_cerrojo);
		if(semaforo->_valor > 0){
			semaforo->_valor--;
			fin++;
		}
		estado|=fjgc_lock_release(semaforo->_cerrojo);
			
		}
	}

	return estado;
}

int fjgc_sem_destroy(struct fjgc_sem* semaforo){

	int estado=fjgc_lock_destroy(semaforo->_cerrojo);
	free(semaforo);
	return estado;
}

struct fjgc_cond{

};

int fjgc_cond_init(struct fjgc_cond** condicion){

	return 0;
}

int fjgc_cond_wait(struct fjgc_cond* condicion, struct fjgc_mutex* cerrojo){

	int estado=fjgc_lock_release(cerrojo);
	estado|=fjgc_lock_acquire(cerrojo);
	return estado;
}

int fjgc_cond_signal(struct fjgc_cond* condicion){

	return 0;
}

int fjgc_cond_broadcast(struct fjgc_cond* condicion){

	return 0;
}

int fjgc_cond_destroy(struct fjgc_cond* condicion){

	return 0;
}
