#ifndef __FJGC_LIB_H__
#define __FJGC_LIB_H__

struct fjgc_hilo;

int fjgc_create_thread(struct fjgc_hilo** hilo, void* (*funcion) (void*), void* argumentos);
int fjgc_join_thread(struct fjgc_hilo* hilo);

struct fjgc_mutex;

int fjgc_lock_init(struct fjgc_mutex** cerrojo);
int fjgc_lock_acquire(struct fjgc_mutex* cerrojo);
int fjgc_lock_release(struct fjgc_mutex* cerrojo);
int fjgc_lock_destroy(struct fjgc_mutex* cerrojo);

//Semaforos en Splash

struct fjgc_sem;

int fjgc_sem_init(struct fjgc_sem** semaforo, int valor);
int fjgc_sem_post(struct fjgc_sem* semaforo);
int fjgc_sem_wait(struct fjgc_sem* semaforo);
int fjgc_sem_destroy(struct fjgc_sem* semaforo);

//Condiciones en Splash

struct fjgc_cond;

int fjgc_cond_init(struct fjgc_cond** condicion);
int fjgc_cond_wait(struct fjgc_cond* condicion, struct fjgc_mutex* cerrojo);
int fjgc_cond_signal(struct fjgc_cond* condicion);
int fjgc_cond_broadcast(struct fjgc_cond* condicion);
int fjgc_cond_destroy(struct fjgc_cond* condicion);

#endif
