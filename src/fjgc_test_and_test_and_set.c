#include <stdlib.h>

#define ENOMEM 12 /*No hay memoria disponible*/

struct fjgc_mutex{
	unsigned char _cerrojo;
};

int fjgc_lock_release(struct fjgc_mutex* cerrojo);

//Inicializa cerrojo a 0
int fjgc_lock_init(struct fjgc_mutex** cerrojo){

	*cerrojo = malloc(sizeof(struct fjgc_mutex));
	if(cerrojo==NULL) return ENOMEM;
	return fjgc_lock_release(*cerrojo);
}

//Implementa la primitiva test_and_test_and_set
int fjgc_lock_acquire(struct fjgc_mutex* cerrojo){

	unsigned char reg1=1;
	unsigned char reg2=0;

	__asm__ __volatile (

		"test_and_set:\n\t"						//Etiqueta de retorno
		"movb %[aCerrojo], %[aReg2]\n\t"		//Carga el contenido de la memoria, puede estar en cache, en el registro
		"cmp $0, %[aReg2]\n\t"					//Comprueba si es posible adquirir el cerrojo
		"jne test_and_set\n\t"					//En caso negativo, vuelve a empezar
		"xchgb %[aReg1], %[aCerrojo]\n\t"	//Intercambia atomicamente los valores del cerrojo y el registro
		"cmp $0, %[aReg1]\n\t"					//Comprueba si el valor del registro es 0
		"jne test_and_set\n\t"					//En caso negativo, vuelve a empezar

		: [aCerrojo] "=m" (*cerrojo)
		: [aReg1] "r" (reg1), [aReg2] "r" (reg2)
		: "cc"
	);

	return 0;
}

//Establece el valor del cerrojo a 0
int fjgc_lock_release(struct fjgc_mutex* cerrojo){

	__asm__ __volatile(

		"movb $0, %[aCerrojo]\n\t"

		: [aCerrojo] "=m" (*cerrojo)
	);

	return 0;
}

//Libera la memoria asociada al cerrojo
int fjgc_lock_destroy(struct fjgc_mutex* cerrojo){

	free(cerrojo);
	return 0;
}
