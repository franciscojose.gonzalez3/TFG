#include <pthread.h>
#include <stdlib.h>
#include "fjgc_lib.h"

#define ENOMEM 12 /*No hay memoria disponible*/

struct fjgc_hilo{
	pthread_t _hilo;
};

int fjgc_create_thread(struct fjgc_hilo** hilo, void* (*funcion) (void*), void* argumentos){

	*hilo = malloc(sizeof(struct fjgc_hilo));
	if(*hilo==NULL) return ENOMEM;
	int error=pthread_create(&(*hilo)->_hilo, NULL, funcion, argumentos);
	if(error){
		free(hilo);
		*hilo=NULL;
	}
	return error;
}

int fjgc_join_thread(struct fjgc_hilo* hilo){

	int error=pthread_join(hilo->_hilo, NULL);
	free(hilo);
	return error;
}
