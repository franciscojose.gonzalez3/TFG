#include <stdlib.h>

#define ENOMEM 12 /*No hay memoria disponible*/

struct nodo {
	volatile int libre;
    volatile struct nodo* sig;
};

struct fjgc_mutex{
	volatile unsigned char _cerrojo;
	volatile struct nodo* inicio;
	volatile struct nodo* fin;
};

//Inicializa cerrojo a 0
int fjgc_lock_init(struct fjgc_mutex** cerrojo){

	*cerrojo = malloc(sizeof(struct fjgc_mutex));
	if(cerrojo==NULL) return ENOMEM;
	(*cerrojo)->_cerrojo=0;
	(*cerrojo)->inicio=NULL;
	(*cerrojo)->fin=NULL;
	return 0;
}

//Implementa la primitiva test_and_set
int fjgc_lock_acquire(struct fjgc_mutex* cerrojo){

	volatile struct nodo* miNodo=malloc(sizeof(struct nodo));
	miNodo->libre=1;
	miNodo->sig=NULL;

	unsigned char reg1=1;

	do{

		__asm__ __volatile (
			"xchg %[aReg1], %[aCerrojo]\n\t"
			: [aCerrojo] "=m" (cerrojo->_cerrojo), [aReg1] "=r" (reg1)
			: "1" (reg1)
		);
	} while(reg1);

	if(cerrojo->inicio==NULL) cerrojo->inicio=miNodo;
	
	if(cerrojo->fin!=NULL){

		cerrojo->fin->sig=miNodo;
		miNodo->libre=0;
	} 

	cerrojo->fin=miNodo;

	cerrojo->_cerrojo=0;

	while(miNodo->libre==0) ;

	return 0;
}

//Establece el valor del cerrojo a 0
int fjgc_lock_release(struct fjgc_mutex* cerrojo){

	unsigned char reg1=1;

	do{

		__asm__ __volatile (
			"xchg %[aReg1], %[aCerrojo]\n\t"
			: [aCerrojo] "=m" (cerrojo->_cerrojo), [aReg1] "=r" (reg1)
			: "1" (reg1)
		);
	} while(reg1);

	volatile struct nodo* miNodo=cerrojo->inicio;

	volatile struct nodo* nodoSig=miNodo->sig;

	if(nodoSig!=NULL) nodoSig->libre=1;

	cerrojo->inicio=nodoSig;

	if(cerrojo->fin==miNodo) cerrojo->fin=NULL;

	free((void*) miNodo);

	cerrojo->_cerrojo=0;

	return 0;
}

//Libera la memoria asociada al cerrojo
int fjgc_lock_destroy(struct fjgc_mutex* cerrojo){

	free(cerrojo);
	return 0;
}
