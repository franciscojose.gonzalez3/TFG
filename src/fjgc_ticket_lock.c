#include <stdlib.h>

#define ENOMEM 12 /*No hay memoria disponible*/

struct fjgc_mutex{
	volatile unsigned long ticketSiguiente;
	volatile unsigned long ticketActual;
};

//Inicializa los tickets a 0
int fjgc_lock_init(struct fjgc_mutex** cerrojo){

	*cerrojo = malloc(sizeof(struct fjgc_mutex));
	if(cerrojo==NULL) return ENOMEM;

	(**cerrojo).ticketSiguiente=0;
	(**cerrojo).ticketActual=0;

	return 0;
}

//Implementa el algoritmo ticket lock
int fjgc_lock_acquire(struct fjgc_mutex* cerrojo){

	unsigned long miTicket=1;

	__asm__ __volatile (
		"lock xadd %[aMiTicket], %[aTicketSiguiente]\n\t"
		: [aTicketSiguiente] "=m" (cerrojo->ticketSiguiente), [aMiTicket] "=r" (miTicket)
		: "1" (miTicket)
	);

	while(miTicket!=cerrojo->ticketActual) ;

	return 0;
}

//Incrementa el ticket actual
int fjgc_lock_release(struct fjgc_mutex* cerrojo){

	cerrojo->ticketActual++;

	return 0;
}

//Libera la memoria asociada al cerrojo
int fjgc_lock_destroy(struct fjgc_mutex* cerrojo){

	free(cerrojo);
	return 0;
}
