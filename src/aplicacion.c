#ifndef _GNU_SOURCE
	#define _GNU_SOURCE
#endif

#include <errno.h>
#include <pthread.h> //pthread_set_affinity
#include <sched.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/sysinfo.h> //get_nprocs
#include <sys/time.h> //gettimeofday
#include "fjgc_lib.h"

#if !_POSIX_C_SOURCE || _POSIX_C_SOURCE < 2
	#define _POSIX_C_SOURCE 2
#endif

#ifndef _XOPEN_SOURCE
	#define _XOPEN_SOURCE
#endif

extern char *optarg;
extern int optind, opterr, optopt;

long ejecuciones=0;
int estado=0;
long long compartido=0;
long sumaCompartida=0;
long sumaPrivada=0;
float porcentajeCritico=0.0;

struct thread_args {
	struct fjgc_hilo* tid;
	unsigned long tidRelativo;
	unsigned long numEjecuciones;
	struct fjgc_mutex* mutex;
};

void* funcion(void *argumentos)
{
	struct thread_args* targs= (struct thread_args*) argumentos;

	long privado=0;

	cpu_set_t cpuset;

	CPU_ZERO(&cpuset);

	pthread_t numcpu = targs->tidRelativo%get_nprocs();

	CPU_SET(numcpu, &cpuset);

	pthread_t ptid = pthread_self();

	if(pthread_setaffinity_np(ptid, sizeof(cpu_set_t), &cpuset)<0){

		fprintf(stderr, "Fallo al establecer afinidad en el hilo %ld: %s\n", targs->tidRelativo, strerror(errno));
		estado|=4;
		exit(estado);
	}

	while(targs->numEjecuciones){

		targs->numEjecuciones--;

		int error=fjgc_lock_acquire(targs->mutex);

		if(error){
			fprintf(stderr, "Fallo al adquirir el lock en el hilo %ld: %s\n", targs->tidRelativo, strerror(error));
			estado|=4;
			exit(estado);
		}

		for(long i=0; i<sumaCompartida; i++){

			__asm__ __volatile("nop\n\t");
			compartido++;
		}

		error=fjgc_lock_release(targs->mutex);
		if(error){
			fprintf(stderr, "Fallo al soltar el lock en el hilo %ld: %s\n", targs->tidRelativo, strerror(error));
			estado|=4;
			exit(estado);
		}

		for(long i=0; i<sumaPrivada; i++){
			__asm__ __volatile("nop\n\t"); //Aseguramos que el compilador no optimiza el bucle
			privado++;
		}

	}

	if(privado!=ejecuciones*sumaPrivada){
		fprintf(stderr, "Fallo en la suma privada del hilo número %ld\n", targs->tidRelativo);
		fprintf(stderr, "Esperado %ld, conseguido %ld\n", ejecuciones*sumaPrivada, privado);
		estado|=16;
	}

	return NULL;
}

int compruebaEntero(long* destino, char* cadena){

	if(cadena==NULL) return -1;
	char* comprobacion=NULL;
	errno=0;
	*destino = strtol(cadena,&comprobacion,10);

	return (errno || comprobacion==cadena || *comprobacion!='\0');
}

int compruebaDecimal(float* destino, char* cadena){

	if(cadena==NULL) return -1;
	char* comprobacion=NULL;
	errno=0;
	*destino = strtod(cadena,&comprobacion);

	return (errno || comprobacion==cadena || *comprobacion!='\0');
}

int main(int argc, char* argv[])
{
	long numHilos=0;
	int primeraVez=0;
	int opt;

	while ((opt = getopt(argc, argv, "h:e:s:p:")) != -1 && estado==0) {
		switch (opt) {
			case 'h':
				if(primeraVez & 1) estado|=1;
				else{
					primeraVez|=1;
					if(compruebaEntero(&numHilos, optarg) || numHilos<=0) estado|=1;
				}
				break;
			case 'e':
				if(primeraVez & 2) estado|=1;
				else{
					primeraVez|=2;
					if(compruebaEntero(&ejecuciones, optarg) || ejecuciones<=0) estado|=1;
				}
				break;
			case 's':
				if(primeraVez & 4) estado|=1;
				else{
					primeraVez|=4;
					if(compruebaEntero(&sumaCompartida, optarg) || sumaCompartida<0) estado|=1;
				}
				break;
			case 'p':
				if(primeraVez & 8) estado|=1;
				else{
					primeraVez|=8;
					if(compruebaDecimal(&porcentajeCritico, optarg) || porcentajeCritico<0.0 || porcentajeCritico>100.0) estado|=1;
				}
				break;
			default: estado|=1;
		}
	}

	if(estado || primeraVez ^ 15 ){

		fprintf(stderr,"Fallo al leer la entrada.\n");
		fprintf(stderr,"El número de hilos ha de ser un número estrictamente positivo\n");
		fprintf(stderr,"El número de ejecuciones ha de ser un número estrictamente positivo\n");
		fprintf(stderr,"El número de sumas ha de ser un número positivo\n");
		fprintf(stderr,"El porcentaje de sumas críticas ha de ser un número entre 0 y 100\n");
		fprintf(stderr,"Cada opción ha de indicarse exactamente una vez\n");
		fprintf(stderr, "Uso: %s -h hilos -e ejecuciones -s sumas -p porcentaje\n", argv[0]);
		return estado;
	}

	sumaPrivada=(100.0-porcentajeCritico)*sumaCompartida/100;
	sumaCompartida-=sumaPrivada;

	struct thread_args* targs= (struct thread_args*) malloc(sizeof(struct thread_args)*numHilos);

	if(targs==NULL){
		fprintf(stderr,"Fallo al reservar memoria\n");
		estado|=2;
		return estado;
	}

	struct fjgc_mutex* cerrojo = NULL;

	int error=fjgc_lock_init(&cerrojo);

	if(error){
		fprintf(stderr, "Fallo al inicializar mutex: %s\n", strerror(error));
		estado|=4;
		return estado;
	}

	//Toma de tiempo inicial
	struct timeval inicio, fin;
	gettimeofday(&inicio, NULL);

	for(long i=0; i<numHilos; i++){

		targs[i].tidRelativo=i+1;
		targs[i].mutex=cerrojo;
		targs[i].numEjecuciones=ejecuciones;
		error=fjgc_create_thread(&targs[i].tid, funcion, &targs[i]);

		if(error){
			fprintf(stderr, "Fallo al crear hilo número %ld: %s\n", i, strerror(error));
			estado|=8;
		}
	}

	for(long i=0; i<numHilos; i++)
		if(targs[i].tid!=NULL){

			error=fjgc_join_thread(targs[i].tid);
			if (error){
				fprintf(stderr, "Fallo al esperar al hilo número %ld: %s\n", i, strerror(error));
				estado|=8;
			}
		}

	gettimeofday(&fin, NULL);

	error=fjgc_lock_destroy(cerrojo);

	if(error){
		fprintf(stderr, "Fallo al destruir mutex: %s\n", strerror(error));
		estado|=4;
		return estado;
	}

	long long resultadoEsperado= numHilos;
	resultadoEsperado*=ejecuciones;
	resultadoEsperado*=sumaCompartida;

	if(compartido != resultadoEsperado){
				fprintf(stderr, "Fallo en la suma compartida, exclusión mutua fallida\n");
				fprintf(stderr, "Esperado %lld, conseguido %lld\n", resultadoEsperado, compartido);
				estado|=16;
	}
	else if(!estado){
		unsigned long long tiempo = fin.tv_sec;
		tiempo-= inicio.tv_sec;
		tiempo*= 1000000;
		tiempo+= fin.tv_usec;
		tiempo-= inicio.tv_usec;
		printf("%lld\n",tiempo);
	}
	else fprintf(stderr, "Ocurrieron errores durante la ejecución\n");

	free(targs);

	return estado;
}
