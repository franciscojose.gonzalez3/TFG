#!/bin/bash

#Fijamos el resto de variables
repeticiones=5
ejecuciones=25000
sumas=10000
hilos=4
porcentaje=100

Salida=Datos_Backoff/$(cat /etc/hostname)_$(date "+%H-%M-%S_%d-%m")
mkdir -p $Salida


#Ejecutamos con las posibles combinaciones
#Valores por defecto: exponente 2, inicio=1, cota sin limite
#Los ponemos los primeros
inicio=(1 10 100)
exponente=(1.5 1.75 2 2.25 2.5)
cota=(NULL 1000 10000 100000 1000000 10000000 100000000 1000000000 10000000000)

export FJGC_INICIO=${inicio[0]}
export FJGC_EXPONENTE=${exponente[0]}
export FJGC_COTA=${cota[0]}


function ejecuta {

	echo "Inicio $FJGC_INICIO, exponente $FJGC_EXPONENTE y cota $FJGC_COTA" | tee -a $Salida/estandar.csv $Salida/errores.txt
	iteracion=0
	./app.test_and_set_backoff -h $hilos -e $ejecuciones -s $sumas -p $porcentaje &> /dev/null

	while [[ $iteracion -lt $repeticiones ]]; do
		./app.test_and_set_backoff -h $hilos -e $ejecuciones -s $sumas -p $porcentaje >> $Salida/estandar.csv 2>> $Salida/errores.txt
		(( iteracion++ ))
	done

	echo "" | tee -a $Salida/estandar.csv $Salida/errores.txt
}

for i in ${inicio[@]}; do
	export FJGC_INICIO=$i
	ejecuta
done

export FJGC_INICIO=${inicio[0]}

for e in ${exponente[@]}; do
	export FJGC_EXPONENTE=$e
	ejecuta
done

export FJGC_EXPONENTE=${exponente[0]}

for c in ${cota[@]}; do
	export FJGC_COTA=$c
	ejecuta
done


