#!/bin/bash

nproc=4
lib=fjgc_libticket_lock

apps/barnes/"$lib"BARNES < apps/barnes/inputs/n16384-p"$nproc"
apps/fmm/"$lib"FMM < apps/fmm/inputs/input."$nproc".16384
apps/ocean/contiguous_partitions/"$lib"OCEAN -p"$nproc" -n258
apps/ocean/non_contiguous_partitions/"$lib"OCEAN -p4 -n258
apps/radiosity/"$lib"RADIOSITY -p "$nproc" -ae 5000 -bf 0.1 -en 0.05 -room -batch
apps/raytrace/"$lib"RAYTRACE -p"$nproc" -m64 apps/raytrace/inputs/car.env 
apps/volrend/"$lib"VOLREND "$nproc" apps/volrend/inputs/head 8
apps/water-nsquared/"$lib"WATER-NSQUARED < apps/water-nsquared/inputs/n512-p"$nproc"
apps/water-spatial/"$lib"WATER-SPATIAL < apps/water-spatial/inputs/n512-p"$nproc"
kernels/cholesky/"$lib"CHOLESKY -p"$nproc" < kernels/cholesky/inputs/tk15.O
kernels/fft/"$lib"FFT -p"$nproc" -m16
kernels/lu/contiguous_blocks/"$lib"LU -p"$nproc" -n512
kernels/lu/non_contiguous_blocks/"$lib"LU -p"$nproc" -n512
kernels/radix/"$lib"RADIX -p"$nproc" -n1048576
