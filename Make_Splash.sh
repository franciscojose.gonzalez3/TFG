#!/bin/bash

function splash {
	cd lib

	export FJGC_LIBDIR=$(pwd)/

	for f in *.a
	do
		export FJGC_LIB=$(basename $f .a)
		make -C ../Splash-3/codes/ $1
	done

	unset FJGC_LIB
	unset FJGC_LIBDIR

	cd ..
}

if [ "$1" == "clean" ]
then
	splash $1
	make $1
else
	make $1
	cp src/fjgc_lib.h lib
	splash $1
fi
