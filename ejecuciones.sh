#!/bin/bash

objetivos=(null pthread test_and_set test_and_set_backoff test_and_test_and_set ticket_lock)

repeticiones=5
ejecuciones=(25000)
sumas=(10000)

hilos=(4 8 16)
porcentajes=$(seq 0 10 100)

Salida=Datos/$(cat /etc/hostname)_$(date "+%H-%M-%S_%d-%m")
mkdir -p $Salida

for o in ${objetivos[@]}; do
	echo $o
	for h in ${hilos[@]}; do
		echo -n "$h "
		for e in ${ejecuciones[@]}; do
			for s in ${sumas[@]}; do
				for p in ${porcentajes[@]}; do

					iteracion=0
					./app.$o -h $h -e $e -s $s -p $p >> /dev/null

					while [[ $iteracion -lt $repeticiones ]]; do
						./app.$o -h $h -e $e -s $s -p $p >> $Salida/$o'_hilos'$h.csv
						(( iteracion++ ))
					done

					echo "" >> $Salida/$o'_hilos'$h.csv
				done
			done
		done
	done
	echo ""
done

hilos=($(seq 1 32))
porcentajes=(10 50 90)

for o in ${objetivos[@]}; do
	echo $o
	for h in ${hilos[@]}; do
		echo -n "$h "
		for e in ${ejecuciones[@]}; do
			for s in ${sumas[@]}; do
				for p in ${porcentajes[@]}; do

					iteracion=0
					./app.$o -h $h -e $e -s $s -p $p >> /dev/null

					while [[ $iteracion -lt $repeticiones ]]; do
						./app.$o -h $h -e $e -s $s -p $p >> $Salida/$o'_porcentaje'$p.csv
						(( iteracion++ ))
					done

					echo "" >> $Salida/$o'_porcentaje'$p.csv
				done
			done
		done
	done
	echo ""
done
